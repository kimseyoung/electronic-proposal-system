function setUpdateNumber(cellValue, options, rowObject) {
    return options.rowId;
}

function radio(cellValue, options, rowObject) {
    var chk =
        '<div class="ui renderer-wrp center singleradio">' + '<input class="ui radio checkbox" name="rad5" type="radio" id=' + "ch" + options.rowId + ">" + "<label for=" + "ch" + options.rowId + ">";
    "</label>" + "</div>";
    return chk;
}

function Link(cellValue, options, rowObject) {
    // col3 + col5 + cellValue 기준 .html을 링크 걸어두었습니다.,
    var baseUrl = "./pages/";
    var url = '<a href="' + baseUrl + rowObject.col3 + "/" + rowObject.col5 + "/" + cellValue + '.html" target="_blank" class="ui renderer-link">' + cellValue + "</a>";
    return url;
}

function modalButtonInput(cellValue, options, rowObject) {
    var renderButton = '<div class="ui renderer-wrp">' + '<input class="ui input pw-100" type="text" >' + '<button class="ui button blue" onclick="opendModal()">확인</button>' + "</div>";
    return renderButton;
}

function singleInput(cellValue, options, rowObject) {
    var singleinput = '<div class="ui renderer-wrp"><input class="ui input pw-100" type="text"></div>';
    return singleinput;
}

function buttonInput(cellValue, options, rowObject) {
    var btninput = '<div class="ui renderer-wrp">' + '<input class="ui input pw-100" type="text">' + '<button class="ui button blue">확인</button>' + "</div>";
    return btninput;
}

function checkbox(cellValue, options, rowObject) {
    var chk =
        '<div class="ui renderer-wrp center">' +
        '<input class="ui input checkbox" type="checkbox" id=' +
        "ch" +
        options.rowId +
        ">" +
        "<label for=" +
        "ch" +
        options.rowId +
        ">" +
        rowObject.col1 +
        "</label>" +
        "</div>";
    return chk;
}

function sigleCheckbox(cellValue, options, rowObject) {
    var chk =
        '<div class="ui renderer-wrp center">' +
        '<input class="ui input checkbox" type="checkbox" name="ch1" id=' +
        "ch" +
        options.rowId +
        ">" +
        "<label for=" +
        "ch" +
        options.rowId +
        ">" +
        "</label>" +
        "</div>";
    return chk;
}

//setId
function _rowId(props) {
    var rowId = $(props).jqGrid("getGridParam").colModel.length;
    var id = { id: rowId, name: "col " + rowId + "" };

    return id;
}

function _rowData(props) {
    var params = $(props).jqGrid("getGridParam");
    console.log("data", params.data);
    var data = params.data;

    return data;
}

//addRowData
function addRow(props) {
    //id, data, position, srcRowId 순서
    $(props).jqGrid("addRow", {
        rowId: _rowId(props).id,
        rowdata: _rowData(props).data,
        position: "last",
    });
}

function removeRow(props) {
    //id, data, position, srcRowId 순서
    var selectionID = $(props).jqGrid("getGridParam", "selrow");
    if (!selectionID) {
        alert("삭제할 데이터 행을 선택해주세요");
    } else {
        $(props).jqGrid("delRowData", selectionID);
    }
}

//인풋 천단위 콤마
function inputNumberFormat(obj) {
    obj.value = comma(uncomma(obj.value));
}

function comma(str) {
    str = String(str);
    return str.replace(/(\d)(?=(?:\d{3})+(?!\d))/g, "$1,");
}

function uncomma(str) {
    str = String(str);
    return str.replace(/[^\d]+/g, "");
}

function colSpan(rowid, val, rowObject, cm, rdata) {
    //전체를 콜스팬할경우
    var result = "";
    var chkcell = { cellId: undefined, chkval: undefined };
    if (chkcell.chkval != val) {
        //check 값이랑 비교값이 다른 경우
        var cellId = this.id + "_row_" + rowid + "-" + cm.name;
        result = ' colspan="6" id ="' + cellId + '" + name="cellRowspan"';
        chkcell = { cellId: cellId, chkval: val };
    } else {
        result = 'style="display:none"  rowspanid="' + chkcell.cellId + '"'; //같을 경우 display none 처리
    }
    return result;
}
// function rowSpan(rowid, val, rowObject, cm, rdata) {
//     var result = "";
//     var chkcell = { cellId: undefined, chkval: undefined };

//     if (chkcell.chkval != val) {
//         //check 값이랑 비교값이 다른 경우
//         var cellId = this.id + "_row_" + rowid + "-" + cm.name;
//         result = ' rowspan="1" id ="' + cellId + '" + name="cellRowspan"';
//         chkcell = { cellId: cellId, chkval: val };
//     } else {
//         result = 'style="display:none"  rowspanid="' + chkcell.cellId + '"'; //같을 경우 display none 처리
//     }
//     return result;
// }

/*
 * @param string grid_id 사이즈를 변경할 그리드의 아이디
 * @param string div_id 그리드의 사이즈의 기준을 제시할 div 의 아이디
 * @param string width 그리드의 초기화 width 사이즈
 * @param boolean tf 그리드의 리사이즈 여부(true/false)
 */
function resizeJqGridWidth(grid_id, div_id, width, tf) {
    // window에 resize 이벤트를 바인딩 한다.
    $(window)
        .bind("resize", function () {
            var resizeWidth = $("#grid_container").width() - 33; //jQuery-ui의 padding 설정 및 border-width값때문에 넘치는 걸 빼줌.

            console.log("????", grid_id, div_id, width, tf);

            // 그리드의 width 초기화
            $("#" + grid_id).setGridWidth(resizeWidth, tf);

            // 그리드의 width를 div 에 맞춰서 적용
            $("#" + grid_id).setGridWidth(resizeWidth, tf); //Resized to new width as per window.
        })
        .trigger("resize");
}
