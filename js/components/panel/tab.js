(function (window) {
  "use strict";
  //tab.js
  //- click으로 할지 tab으로 할지 요건이 불분명 화면전환을 하여도 화면이 똑같은 형태이기 때문에, 2가지 (click & tab) 유형을 작성 하였음...

  const panes = function (params) {
    /**
     * @param {string} tabElement id 부모요소 식별
     * @param {string} tabsChildren id 내부 자식의 클릭할 element
     * @param {string} tabsContent 전환할 컨텐츠의 element
     * @param {string} tabsActive tab의 active class
     * @param {boolean} switching 컨텐츠 전환 유무
     */

    const tab = document.getElementById(params.tabElement);
    const tabsChildren = tab.querySelectorAll(params.tabsChildren);
    const tabsContent = document.querySelectorAll(params.tabsContents);
    const tabsActive = params.tabsActive;
    const switching = params.switching;
    let defaults = {
      tabElement: tab,
      id: params.id,
      tabsChildren: tabsChildren,
      tabsContent: tabsContent,
      tabsActive: tabsActive,
      activeIndex: params.activeIndex ? params.activeIndex : 0, //0 부터 첫번째
      switching: switching,
    };

    this.defaults = defaults;
    this.init();
  };

  panes.prototype.init = function () {
    this.publicFunctions();
    this.addEvents();
  };

  panes.prototype.addEvents = function () {
    if (document.addEventListener) {
      document.addEventListener("click", this.tabsChildrenClicked, false);
      document.addEventListener(
        "DOMContentLoaded",
        this.defaultSettings,
        false
      );
    }

    return this;
  };

  panes.prototype.publicFunctions = function () {
    const _self = this;
    const _index = this.defaults.activeIndex;
    const _active = this.defaults.tabsActive;
    const _swich = this.defaults.switching;

    this.defaultSettings = function () {
      //default setting
      if (typeof _swich === Boolean && _swich === false) {
        return false;
      } else {
        _self.defaults.tabsChildren[_index].classList.add(_active);
        if (_self.defaults.tabsContent[_index] === undefined) {
          return false;
        } else {
          _self.defaults.tabsContent[_index].style.display = "block";
        }
      }

      return this;
    };
    this.tabsChildrenClicked = function (e) {
      //tabs clicked
      if (!e.target.rel) {
        return false;
      }
      const rel = e.target.rel.replace(/[0-9]/g, "");

      if (rel) {
        [].forEach.call(
          _self.defaults.tabsChildren,
          function (isTarget, index) {
            let target = e.target.parentElement;
            if (target === isTarget) {
              //parentTarget과 isTarget이 같을때 실행
              const getId = e.target.getAttribute("href", "").replace("#", "");
              e.target.parentElement.classList.add(_active);

              if (_swich === false) {
                return false;
              } else {
                const getContent = _self.defaults.tabsContent[index].id;
                getId === getContent
                  ? (_self.defaults.tabsContent[index].style.display = "block")
                  : undefined;
              }
            } else {
              if (rel === _self.defaults.id) {
                isTarget.classList.remove(_active);
                if (_swich === false) {
                  return false;
                } else {
                  _self.defaults.tabsContent[index].style.display = "none";
                }
              } else {
                return false;
              }
              return false;
            }
          }
        );
      } else {
        return false;
      }
      _self.removeEvents();

      return this;
    };

    this.removeEvents = function () {
      if (document.addEventListener) {
        document.removeEventListener("click", this.tabsChildrenClicked, false);
      }

      setTimeout(function () {
        _self.addEvents();
      }, 100);
    };
  };

  window.panes = panes;
})(window);
